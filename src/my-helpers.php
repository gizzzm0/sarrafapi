<?php
//Helper functions

//Base64 string to image
function base64_to_jpeg($base64_string, $img_dir, $length = 20)
{
    $data = explode(',', $base64_string);
    $filename = createName($length) . '.jpg';
    if (count($data) > 1) {
        file_put_contents($img_dir . $filename, base64_decode($data[1]));
    }
    return $filename;
}

function createName($len)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet .= "0123456789";
    $max = strlen($codeAlphabet);

    for ($i = 0; $i < $len; $i++) {
        //$token .= $codeAlphabet[random_int(0, $max - 1)]; Use this for PHP 7+
        $token .= $codeAlphabet[crypto_rand_secure(0, $max - 1)];
    }

    return $token;
}

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int)($log / 8) + 1; // length in bytes
    $bits = (int)$log + 1; // length in bits
    $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd > $range);
    return $min + $rnd;
}

function fxCalc($toSend, $rate)
{
    return number_format(floatval($toSend)/floatval($rate), 6);
}

function sendTxnSMS($api, $phone, $message)
{
    $result = [];
    try {
        $status = $api->sendMessage($phone, $message);
        //Add User
        if ($status[0]->status == 'Success') {
            $result['sent'] = true;
        } else {
            $result['sent'] = false;
            $result['msg'] = $status[0]->status;
        }
    } catch (AfricasTalkingGatewayException $e) {
        $result['sent'] = false;
        $result['msg'] = 'Couldn\'t send Transaction PIN. Please try again.';
    }
    return $result;
}

function sendConfirmationSMS($api, $phone, $message)
{
    $result = [];
    try {
        $status = $api->sendMessage($phone, $message);
        //Add User
        if ($status[0]->status == 'Success') {
            $result['sent'] = true;
        } else {
            $result['sent'] = false;
            $result['msg'] = $status[0]->status;
        }
    } catch (AfricasTalkingGatewayException $e) {
        $result['sent'] = false;
        $result['msg'] = 'Couldn\'t send Transaction Confirmation Message! Please try again.';
    }
    return $result;
}

function sendPushNotification($message, $accountID, $accountAmt, $playerID)
{
    $content = array(
        "en" => $message
    );

    $fields = array(
        'app_id' => "7d261983-8ece-45ca-9c2d-8b553459d5a7",
        'include_player_ids' => array($playerID),
        'data' => array("accID" => $accountID, "accAmt" => $accountAmt),
        'contents' => $content
    );

    $fields = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
        'Authorization: Basic ODg4ZTFlNjYtOGUxYS00YmNlLTliOWEtYzI5OTQ1OTI2NGVi'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}

?>