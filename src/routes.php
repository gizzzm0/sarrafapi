<?php
require_once("database.php");
require_once('sms-gateways/AfricasTalkingGateway.php');
require_once('my-helpers.php');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Firebase\JWT\JWT;

/*****************************************************************
 ******************** USER ACCOUNT ACTIVITIES ********************
 ****************************************************************/

// Register New Account
$app->post('/v1/register', function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    $Remote = security($_SERVER['REMOTE_ADDR']);
    $RemoteBrowser = security($_SERVER['HTTP_USER_AGENT']);
    $result = [];

    $user_exists = select('tbl_system_users')->where('userEmail = "' . security($data['email']) . '" OR phoneNumber ="' . security($data['countrycode'] . $data['phone']) . '"')->num_rows();
    if ($user_exists < 1) {
        $alphaNumeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $ShuffleString = str_shuffle($alphaNumeric);
        $SMSCode = substr($ShuffleString, 0, 7);

        if (substr($data['phone'], 0, 1) === "0") {
            $data['phone'] = substr($data['phone'], 1);
        }

        $values = array(
            'userGroupID' => 2,
            'firstName' => security($data['firstname']),
            'middleName' => security($data['middlename']),
            'lastName' => security($data['surname']),
            'userEmail' => security($data['email']),
            'dateOfBirth' => security($data['dob']),
            'idPPNumber' => security($data['idpp']),
            'countryID' => security($data['countryid']),
            'phoneNumber' => security($data['countrycode'] . $data['phone']),
            'password' => md5($SMSCode),
            'UIP' => $Remote,
            'UPC' => $RemoteBrowser,
            'defaultCurrency' => security($data['currency']),
            'pushID' => security($data['pushID'])
        );

        //Send SMS
        $username = "Acculynk";
        $apikey = "2961d831076d8a4abcb0218075dfaaaba267d7d7ebfb839889cfdc754594f8e7";
        $recipients = preg_replace('/\s+/', '', $data['countrycode'] . $data['phone']);
        $message = 'Dear ' . $data['firstname'] . ', Your One Time Surraf Activation PIN is ' . $SMSCode . '.';
        $gateway = new AfricasTalkingGateway($username, $apikey);
        try {
            $status = $gateway->sendMessage($recipients, $message);
            //Add User
            if ($status[0]->status == 'Success') {
                insert('tbl_system_users')->values($values);
                $lastInsertID = last_id();
                if ($lastInsertID > 0) {
                    $result['success'] = true;
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Oops! There was a problem creating your account. Please try again or contact Surraf customer service.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = $status[0]->status;
            }
        } catch (AfricasTalkingGatewayException $e) {
            $result['success'] = false;
            $result['msg'] = 'Couldn\'t send Activation PIN. Please try again.';
        }

    } else {
        $result['success'] = false;
        $result['msg'] = 'A user with your Phone Number or Email already exists. Please try resetting your password.';
    }
    return $this->response->withJson($result);
});

//Confirm Registration
$app->post('/v1/confirm', function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    $result = [];

    $qry = select('tbl_system_users')->which('id, firstName, lastName, middleName, userAvatar, defaultCurrency, countryID')->where('password = "' . md5($data['pin']) . '" AND isActive = 0');
    $user_exists = $qry->result();
    if ($qry->num_rows() > 0) {
        $uid = $user_exists['id'];
        $acc_name = $user_exists['lastName'] . ' ' . $user_exists['firstName'] . ' ' . $user_exists['middleName'];
        $currency = $user_exists['defaultCurrency'];
        $country = $user_exists['countryID'];
        $password = md5($data['password']);
        $passconfirm = md5($data['cpassword']);
        if ($password == $passconfirm) {
            //Activate User
            update('tbl_system_users')->values(array('password' => $password, 'isActive' => 1))->where('password = "' . md5($data['pin']) . '" AND isActive = 0');
            //Add Default Account
            $account = array(
                'isDefaultAccount' => 1,
                'accountPIN' => md5($data['pin']),
                'accountTypeID' => 3,
                'accountTypeDesc' => 'Surraf Default Wallet',
                'accountName' => $acc_name,
                'accountDescription' => 'Surraf Virtual Wallet Default Account',
                'accountNumber' => 'S' . $uid,
                'userID' => $uid,
                'canBeEdited' => 0,
                'accountCurrencyID' => $currency,
                'accountCountryID' => $country
            );
            insert('tbl_user_money_accounts')->values($account);
            $lastInsertID = last_id();
            $accdata = select('tbl_user_money_accounts')->which('id AS accountID, isDefaultAccount AS isDefault,ROUND(accountActualBalance, 2) AS AmountAvailable, accountTypeDesc AS Type, accountCurrencyID AS Currency, accountNumber AS Number, accountName AS Name')->where("id =  $lastInsertID")->result();
            $token = select('tbl_access_tokens')->which('value')->where("user_id = $uid AND expiration_date > " . time())->result();
            if (!empty($token)) {
                $user_exists['token'] = $token['value'];
                $result['success'] = true;
            } else {
                $key = 'my_secret_key';
                $payload = array(
                    "iss" => "Surraf",
                    "iat" => time(),
                    "exp" => time() + (3600 * 24 * 15),
                    "context" => [
                        "user" => [
                            "user_login" => $user_exists['firstName'] . ' ' . $user_exists['lastName'] . ' ' . $user_exists['middleName'],
                            "user_id" => $user_exists['id']
                        ]
                    ]
                );
                try {
                    $jwt = JWT::encode($payload, $key);
                    $values = array('value' => $jwt,
                        'user_id' => $uid,
                        'date_created' => $payload['iat'],
                        'expiration_date' => $payload['exp']);
                    insert('tbl_access_tokens')->values($values);
                    $user_exists['token'] = $jwt;
                    $result['success'] = true;
                    $result['account'] = $accdata;
                } catch (Exception $e) {
                    $result['success'] = false;
                    $result['msg'] = $e->getMessage();
                }
            }
            $result['data'] = $user_exists;
        } else {
            $result['success'] = false;
            $result['msg'] = 'Sorry, your passwords do not match!';
        }
    } else {
        $result['success'] = false;
        $result['msg'] = 'Sorry, you have entered the wrong Activation PIN.';
    }
    return $this->response->withJson($result);
});

// Authenticate Login and Assign Token
$app->post('/v1/auth', function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    $result = [];

    $login = security($data['phone']);
    $passwd = md5($data['passcode']);

    $user = select('tbl_system_users')->which('id, firstName, lastName, middleName, userAvatar')->where('phoneNumber = "' . $login . '" AND password = "' . $passwd . '" AND isActive = 1')->result();
    if (!empty($user)) {
        $uid = $user['id'];
        $token = select('tbl_access_tokens')->which('value')->where("user_id = $uid AND expiration_date > " . time())->result();
        $accounts = select('tbl_user_money_accounts')->which('id AS accountID, isDefaultAccount AS isDefault, ROUND(accountActualBalance, 2) AS AmountAvailable, accountTypeDesc AS Type, accountCurrencyID AS Currency, accountNumber AS Number, accountName AS Name')->where("isActive = 1 AND userID = $uid")->results();
        $result['accounts'] = $accounts;
        if (!empty($token)) {
            $ll = date('Y-m-d H:i:s');
            update('tbl_system_users')->values(array('lastLogin'=>$ll))->where("id = $uid");
            $user['token'] = $token['value'];
            $result['success'] = true;
        } else {
            $key = 'my_secret_key';
            $payload = array(
                "iss" => "Surraf",
                "iat" => time(),
                "exp" => time() + (3600 * 24 * 15),
                "context" => [
                    "user" => [
                        "user_login" => $user['firstName'] . ' ' . $user['lastName'] . ' ' . $user['middleName'],
                        "user_id" => $user['id']
                    ]
                ]
            );
            try {
                $jwt = JWT::encode($payload, $key);
                $values = array('value' => $jwt,
                    'user_id' => $uid,
                    'date_created' => $payload['iat'],
                    'expiration_date' => $payload['exp']);
                insert('tbl_access_tokens')->values($values);
                $user['token'] = $jwt;
                $result['success'] = true;
            } catch (Exception $e) {
                $result['success'] = false;
                $result['msg'] = $e->getMessage();
            }
        }
        $result['data'] = $user;
    } else {
        $result['success'] = false;
        $result['msg'] = "Please provide the correct credentials to Sign In";
    }
    return $this->response->withJson($result);
});

// Reset Password. ToDo: Send URL to user email with password reset link
$app->post('/v1/reset', function (Request $request, Response $response) {
    $result = [];
    $result['success'] = true;
    return $this->response->withJson($result);
});

// Get User Info.
$app->get('/v1/my_profile', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $result['success'] = true;
                $userData = select('tbl_system_users U')->which('U.id, U.firstName, U.middleName, U.lastName, U.userEmail, U.dateOfBirth, U.countryID, U.userAvatar, C.calling_code AS countryCode, C.short_name AS countryName, U.phoneNumber, U.defaultCurrency, U.password AS passHash')->left('conf_countries C ON U.countryID = C.id')->where("U.id = $UID")->result();
                $countryCode = $userData['countryCode'];
                $phoneNumber = $userData['phoneNumber'];
                $pos = strpos($phoneNumber, $countryCode, 0);
                if ($pos !== false) {
                    $userData['phoneNumber'] = substr_replace($phoneNumber, '', $pos, strlen($countryCode));
                }
                $result['udata'] = $userData;
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

// Get account edit OTP
$app->post('/v1/edit_verification', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $data = $request->getParsedBody()[0];
    $result = [];

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $user_exists = select('tbl_system_users')->where('(userEmail = "' . security($data['newEmail']) . '" OR phoneNumber ="' . security($data['newPhoneNoo']) . '") AND id != ' . $UID)->num_rows();
                if ($user_exists < 1) {
                    $alphaNumeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                    $ShuffleString = str_shuffle($alphaNumeric);
                    $SMSCode = substr($ShuffleString, 0, 7);

                    $values = array(
                        'accountEditOTP' => md5($SMSCode)
                    );

                    //Send SMS
                    $username = "Acculynk";
                    $apikey = "2961d831076d8a4abcb0218075dfaaaba267d7d7ebfb839889cfdc754594f8e7";
                    $recipients = preg_replace('/\s+/', '', $data['newPhoneNo']);
                    $message = 'Dear ' . ucfirst($data['newFirstName']) . ', Your One Time Surraf Activation PIN is ' . $SMSCode . '.';
                    $gateway = new AfricasTalkingGateway($username, $apikey);
                    try {
                        $status = $gateway->sendMessage($recipients, $message);
                        //Add User
                        if ($status[0]->status == 'Success') {
                            update('tbl_system_users')->values($values)->where("id = $UID");
                            $result['success'] = true;
                            $result['otpHash'] = md5($SMSCode);

                        } else {
                            $result['success'] = false;
                            $result['msg'] = $status[0]->status;
                        }
                    } catch (AfricasTalkingGatewayException $e) {
                        $result['success'] = false;
                        $result['msg'] = 'Couldn\'t send Activation PIN. Please try again.' . $e->getMessage();
                    }

                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Sorry! A different user with your Phone Number or Email already exists.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }

    return $this->response->withJson($result);
});

// Edit Account
$app->post('/v1/edit_profile', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $data = $request->getParsedBody();
    $result = [];
    $Remote = security($_SERVER['REMOTE_ADDR']);
    $RemoteBrowser = security($_SERVER['HTTP_USER_AGENT']);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $user_exists = select('tbl_system_users')->where('(userEmail = "' . security($data['newEmail']) . '" OR phoneNumber ="' . security($data['newPhoneNo']) . '") AND id != ' . $UID)->num_rows();
                if ($user_exists < 1) {
                    $pin_exists = select('tbl_system_users')->where("id = $UID AND accountEditOTP IS NOT NULL")->num_rows();
                    if ($pin_exists > 0) {
                        if (substr($data['phone'], 0, 1) === "0") {
                            $data['phone'] = substr($data['phone'], 1);
                        }

                        $values = array(
                            'userGroupID' => 2,
                            'firstName' => security($data['firstname']),
                            'middleName' => security($data['middlename']),
                            'lastName' => security($data['surname']),
                            'userEmail' => security($data['email']),
                            'dateOfBirth' => security($data['dob']),
                            'countryID' => security($data['countryid']),
                            'phoneNumber' => security($data['countrycode'] . $data['phone']),
                            'UIP' => $Remote,
                            'UPC' => $RemoteBrowser,
                            'accountEditOTP' => NULL
                        );
                        //Add User
                        update('tbl_system_users')->values($values)->where("id = $UID AND accountEditOTP IS NOT NULL");
                        $result['success'] = true;
                        $result['msg'] = 'Profile successfully saved.';
                    } else {
                        $result['success'] = false;
                        $result['msg'] = 'Wrong Activation PIN! Please try again or request for another!';
                    }
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Sorry! A different user with your Phone Number or Email already exists.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch
    (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }

    return $this->response->withJson($result);
});

// Change Avatar
$app->post('/v1/upload_avi', function (Request $request, Response $response) {
    $aviPath = '../../avis/';
    $urlPath = 'http://sarraf.acculynksystems.com/avis/';
    $key = "my_secret_key";
    $jwt = $request->getHeaders();
    $now = time();
    $data = $request->getParsedBody();
    $result = [];

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $user_exists = select('tbl_system_users')->where("id = $UID AND isActive = 1")->num_rows();
                if ($user_exists == 1) {
                    $filename = $urlPath . base64_to_jpeg(str_replace("\n", "", $data['aviEncode']), $aviPath);
                    $values = array(
                        'userAvatar' => $filename
                    );
                    update('tbl_system_users')->values($values)->where("id = $UID AND isActive = 1");
                    //ToDo: Unlink existing avi
                    $result['success'] = true;
                    $result['msg'] = 'Avatar successfully saved.';
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Sorry! A couldn\'t find your user account. Please contact Surraf customer service.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch
    (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }

    return $this->response->withJson($result);
});

//Change Password
$app->post('/v1/change_pass', function (Request $request, Response $response) {
    $key = "my_secret_key";
    $jwt = $request->getHeaders();
    $now = time();
    $data = $request->getParsedBody();
    $result = [];
    $pass = md5($data['currentpin']);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $user_exists = select('tbl_system_users')->where("id = $UID AND isActive = 1 AND password = '" . $pass . "'")->num_rows();
                if ($user_exists == 1) {
                    $values = array(
                        'password' => md5($data['newpin']),
                        'lastPasswordChange' => date('Y-m-d H:i:s')
                    );
                    update('tbl_system_users')->values($values)->where("id = $UID AND isActive = 1 AND password = '" . $pass . "'");
                    $result['success'] = true;
                    $result['msg'] = 'Passcode successfully changed.';
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Sorry! A couldn\'t find your user account or Wrong Passcode. Please contact Surraf customer service.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch
    (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }

    return $this->response->withJson($result);
});

/*****************************************************************
 ********************* ACCOUNTS ACTIVITIES ***********************
 ****************************************************************/

//Get banks per country selected
$app->get('/v1/banks', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $banks = select('tbl_banks B')->which('B.id AS bankID, bankName')->left('tbl_system_users SU ON SU.countryID = B.bankCountryID')->where("B.isActive = 1 AND SU.id = $UID")->order('bankName ASC')->results();
                $result['success'] = true;
                $result['data'] = $banks;
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

//Get branch per bank selected
$app->get('/v1/bank_branches', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $mainBankID = intval($request->getQueryParams()['bankID']);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $banks = select('tbl_bank_branches')->which('id AS branchID, bankBranchName AS branchName')->where("isActive = 1 AND bankID = $mainBankID")->order('bankBranchName ASC')->results();
                $result['success'] = true;
                $result['data'] = $banks;
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

//Get telcos per country selected
$app->get('/v1/telcos', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $telcos = select('tbl_telcos T')->which('T.id AS telcoID, telcoName')->left('tbl_system_users SU ON SU.countryID = T.telcoCountryID')->where("T.isActive = 1 AND SU.id = $UID")->order('telcoName ASC')->results();
                $result['success'] = true;
                $result['data'] = $telcos;
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

//Add new account
$app->post('/v1/new_account', function (Request $request, Response $response) {
    $key = "my_secret_key";
    $jwt = $request->getHeaders();
    $now = time();
    $data = $request->getParsedBody();
    $result = [];
    $accountType = security($data['type']);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');
            if ($UID > 0) {
                $userqry = select('tbl_system_users')->which('id, countryID')->where("id = $UID AND isActive = 1");
                $user_exists = $userqry->num_rows();
                $user_deets = $userqry->result();

                if ($user_exists == 1) {
                    //Prepare account data
                    $account = array(
                        'isDefaultAccount' => 0,
                        'accountPIN' => md5($data['pin']),
                        'accountTypeID' => $accountType,
                        'accountName' => security($data['name']),
                        'accountDescription' => security($data['desc']),
                        'userID' => $UID,
                        'canBeEdited' => 1,
                        'accountCurrencyID' => security($data['currency']),
                        'accountCountryID' => $user_deets['countryID']
                    );
                    if ($accountType == 1) {
                        $accountNo = security($data['accnumber']);
                        $account['accountNumber'] = $accountNo;
                        $account['accountBankID'] = security($data['bank']);
                        $account['accountBranchID'] = security($data['branch']);
                        $account['accountTypeDesc'] = 'Bank';
                    } elseif ($accountType == 2) {
                        $accountNo = security($data['phonenumber']);
                        $account['accountNumber'] = $accountNo;
                        $account['accountTypeDesc'] = 'Telco';
                        $account['accountTelcoID'] = security($data['telco']);
                    } elseif ($accountType == 3) {
                        $default = select('tbl_user_money_accounts')->which('accountNumber')->where("userID = $UID AND isDefaultAccount = 1")->result('accountNumber');
                        $accountNo = $default . security($data['currency']);
                        $account['accountNumber'] = $accountNo;
                        $account['accountTypeDesc'] = 'Surraf Wallet';
                        $account['accountDescription'] = 'Surraf Virtual Wallet Account';
                    } elseif ($accountType == 4) {
                        $accountNo = security($data['cardnumber']);
                        $account['accountNumber'] = $accountNo;
                        $account['accountCardCVC'] = security($data['cvc']);
                        $account['accountCardExpiry'] = security($data['cardexpiry']);
                        $account['accountTypeDesc'] = 'Card';
                    } elseif ($accountType == 5) {
                        $accountNo = security($data['ppemail']);
                        $account['accountNumber'] = $accountNo;
                        $account['accountTypeDesc'] = 'Paypal';
                    }
                    //Search if account exists
                    $account_exists = select('tbl_user_money_accounts')->where("userID = $UID AND accountNumber = '" . $accountNo . "' AND accountCountryID = '" . $user_deets['countryID'] . "' AND accountTypeID = $accountType")->num_rows();
                    if ($account_exists == 0) {
                        insert('tbl_user_money_accounts')->values($account);
                        $lastInsertID = last_id();
                        if ($lastInsertID > 0) {
                            $result['success'] = true;
                            $data = select('tbl_user_money_accounts')->which('id AS accountID, isDefaultAccount AS isDefault,ROUND(accountActualBalance, 2) AS AmountAvailable, accountTypeDesc AS Type, accountCurrencyID AS Currency, accountNumber AS Number, accountName AS Name')->where("id =  $lastInsertID")->result();
                            $result['msg'] = 'Account Successfully Created.';
                            $result['data'] = $data;
                        } else {
                            $result['success'] = false;
                            $result['msg'] = 'There was a problem creating your account. Please try again or contact Surraf customer support.';
                        }
                    } else {
                        $result['success'] = false;
                        $result['msg'] = 'Account Already Exists.';
                    }
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Sorry! A couldn\'t find your user account. Please contact Surraf customer service.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch
    (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }

    return $this->response->withJson($result);
});

//Get account info
$app->get('/v1/account_info', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $accountID = intval($request->getQueryParams()['accID']);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $account = select('tbl_user_money_accounts')->which('id as accID, isDefaultAccount, accountTypeID, accountName, accountNumber, accountDescription, accountPIN, accountCurrencyID')->where("isActive = 1 AND id = $accountID AND canBeEdited = 1")->result();
                if (isset($account) AND !empty($account) AND count($account) > 0) {
                    $result['success'] = true;
                    $result['data'] = $account;
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'You cannot edit this account';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

//Edit account
$app->post('/v1/edit_account', function (Request $request, Response $response) {
    $key = "my_secret_key";
    $jwt = $request->getHeaders();
    $now = time();
    $data = $request->getParsedBody();
    $result = [];
    $accID = $data['id'];

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');
            if ($UID > 0) {
                if ($data['pin'] === $data['cpin']) {
                    $user_exists = select('tbl_system_users')->where("id = $UID AND isActive = 1")->num_rows();

                    if ($user_exists == 1) {
                        //Prepare account data
                        $account = array(
                            'accountName' => security($data['name']),
                            'accountDescription' => security($data['desc']),
                            'accountNumber' => security($data['number']),
                            'accountCurrencyID' => security($data['currency']),
                        );
                        if (!empty($data['pin'])) {
                            $account['accountPIN'] = md5($data['pin']);
                        }
                        update('tbl_user_money_accounts')->values($account)->where("id = $accID");
                        $result['success'] = true;
                        $result['msg'] = 'Account Successfully Updated.';
                    } else {
                        $result['success'] = false;
                        $result['msg'] = 'Sorry! A couldn\'t find your user account. Please contact Surraf customer service.';
                    }
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'Oops! Your new PINs DO NOT match. Please correct and resubmit.';
            }
        }
    } catch
    (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }

    return $this->response->withJson($result);
});

//Get user accounts
$app->get('/v1/user_txn_accounts', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $userEmail = trim($request->getQueryParams()['userEmail']);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $recipientID = select('tbl_system_users')->which('id')->where("userEmail = '" . $userEmail . "' AND isActive = 1")->result('id');
                if ($recipientID > 0) {
                    $accounts = select('tbl_user_money_accounts')->which('id as accountID, accountName, accountCurrencyID')->where("isActive = 1 AND userID = $recipientID")->results();
                    if (isset($accounts) AND !empty($accounts) AND count($accounts) > 0) {
                        $result['success'] = true;
                        $result['data'] = $accounts;
                    } else {
                        $result['success'] = false;
                        $result['msg'] = 'Recipient has no configured accounts';
                    }
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Recipient account not be found. Please confirm the email and search again or contact Surraf customer support. Thanks.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

//Get available balance
$app->get('/v1/account_balance', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $accountID = intval($request->getQueryParams()['accID']);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                $balance = select('tbl_user_money_accounts')->which('accountActualBalance as accBalance, accountCurrencyID as accCurr')->where("isActive = 1 AND id = $accountID")->result();
                $result['success'] = true;
                $result['data'] = $balance;
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

/*****************************************************************
 ******************** TRANSACTION ACTIVITIES *********************
 ****************************************************************/

//Get fx rate
$app->get('/v1/get_fx_rate_main', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $sendingCurr = security($request->getQueryParams()['sendingCurr']);
    $receivingCurr = security($request->getQueryParams()['receivingCurr']);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                if ($sendingCurr == $receivingCurr) {
                    $rate['rate'] = 1;
                } else {
                    $rate = select('conf_fx_rates')->which('sellingRate as rate')->where("baseCurrency = '" . $sendingCurr . "' AND currency ='" . $receivingCurr . "'")->result();
                }
                if (isset($rate) AND !empty($rate)) {
                    $result['success'] = true;
                    $result['data'] = $rate;
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Sorry! Transaction between these currencies isn\'t active yet. Please check back later. Thank you.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

//Get fx rate
$app->get('/v1/get_fx_rate', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $sendingAcc = intval($request->getQueryParams()['sendingID']);
    $receivingAcc = intval($request->getQueryParams()['receivingID']);
    $sendingCurr = select('tbl_user_money_accounts')->which('accountCurrencyID')->where("id =  $sendingAcc AND isActive = 1")->result('accountCurrencyID');
    $receivingCurr = select('tbl_user_money_accounts')->which('accountCurrencyID')->where("id =  $receivingAcc AND isActive = 1")->result('accountCurrencyID');

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                if ($sendingCurr == $receivingCurr) {
                    $rate['rate'] = 1;
                } else {
                    $rate = select('conf_fx_rates')->which('sellingRate as rate')->where("baseCurrency = '" . $sendingCurr . "' AND currency ='" . $receivingCurr . "'")->result();
                }
                if (isset($rate) AND !empty($rate)) {
                    $rate['send'] = $sendingCurr;
                    $rate['receive'] = $receivingCurr;
                    $result['success'] = true;
                    $result['data'] = $rate;
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Sorry! Transaction between these currencies isn\'t active yet. Please check back later. Thank you.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

//Send money
$app->post('/v1/send_money', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $data = $request->getParsedBody();
    $sendingAccID = intval($data['SendAcc']);
    $receivingAccID = intval($data['RecAcc']);
    $amtToSend = floatval(str_replace(',', '', $data['SendAmt']));
    $transactionCategory = 1; //SurrafAcccounts

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');

            if ($UID > 0) {
                //Sending Account Details
                $sendingAcc = select('tbl_user_money_accounts')->where("id = $sendingAccID AND isActive = 1")->result();
                $sendingAccBalance = $sendingAcc['accountActualBalance'];
                $sendingAccCurrency = $sendingAcc['accountCurrencyID'];
                $sendingAccType = $sendingAcc['accountTypeID'];
                $sendingAccUID = $sendingAcc['userID'];
                $sendingAccUPhone = select('tbl_system_users')->which('phoneNumber')->where("id = $sendingAccUID AND isActive = 1")->result('phoneNumber');
                //Receiving Account Details
                $receivingAcc = select('tbl_user_money_accounts')->where("id = $receivingAccID AND isActive = 1")->result();
                $receivingAccCurrency = $receivingAcc['accountCurrencyID'];
                $receivingAccType = $receivingAcc['accountTypeID'];
                //Check if user has enough funds to transact
                if (isset($sendingAcc) AND !empty($sendingAcc) AND count($sendingAcc) > 0 AND isset($receivingAcc) AND !empty($receivingAcc) AND count($receivingAcc) > 0) {
                    if ($sendingAccBalance >= $amtToSend) {
                        //Prepare for transaction
                        $InActiveMsg = 'Sorry! Transaction between these accounts isn\'t active yet. Please check back later. Thank you.';
                        // Create Transaction Reference
                        $str12 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                        $shuffled12 = str_shuffle($str12);
                        $DUniqueReference = substr($shuffled12, 0, 6);
                        $GetTxnSeries = select('tbl_code_serialization')->which('codeSeries')->order('id DESC')->limit(1)->result('codeSeries');
                        $TxnReference = $DUniqueReference . $GetTxnSeries;
                        $SMSCode = substr(str_shuffle($str12), 0, 7) . $GetTxnSeries;
                        $values = array(
                            'txnReference' => $TxnReference,
                            'fromAccount' => $sendingAccID,
                            'fromAccountType' => $sendingAccType,
                            'toAccount' => $receivingAccID,
                            'toAccountType' => $receivingAccType,
                            'trxAmount' => $amtToSend,
                            'trxType' => 1,
                            'trxFromCurrency' => $sendingAccCurrency,
                            'trxToCurrency' => $receivingAccCurrency,
                            'trxActualAmount' => $amtToSend,
                            'trxActualCommission' => 0,
                            'trxStatus' => 0,
                            'MERCHANT_TRANSACTION_ID' => 1,
                            'transactionOTP' => md5($SMSCode)
                        );
                        $username = "Acculynk";
                        $apikey = "2961d831076d8a4abcb0218075dfaaaba267d7d7ebfb839889cfdc754594f8e7";
                        $gateway = new AfricasTalkingGateway($username, $apikey);
                        $message = "Your One Time Surraf Payment Solutions transaction PIN is " . $SMSCode . ".";
                        $rate = select('conf_fx_rates')->which('sellingRate')->where("baseCurrency = '" . $sendingAccCurrency . "' AND currency ='" . $receivingAccCurrency . "'")->result('sellingRate');

                        //Surraf Accounts Txn
                        if ($transactionCategory == 1) {
                            //Surraf Wallet to Surraf Wallet
                            if ($sendingAccType == 3 AND $receivingAccType == 3) {
                                if ($sendingAccCurrency == $receivingAccCurrency) {
                                    //Send OTP SMS
                                    $sms = sendTxnSMS($gateway, $sendingAccUPhone, $message);
                                    if ($sms['sent'] == true) {
                                        insert('tbl_transactions')->values($values);
                                        $lastInsertID = last_id();
                                        if ($lastInsertID > 0) {
                                            $result['success'] = true;
                                        } else {
                                            $result['success'] = false;
                                            $result['msg'] = 'Sorry! Couldn\'t complete your transaction. Please contact Surraf customer service';
                                        }
                                    } else {
                                        $result['success'] = false;
                                        $result['msg'] = $sms['msg'];
                                    }
                                } else {
                                    $fxAmtToSend = fxCalc($amtToSend, $rate);
                                    if($rate > 0) {
                                        $values['trxActualAmount'] = $fxAmtToSend;
                                        //Send OTP SMS
                                        $sms = sendTxnSMS($gateway, $sendingAccUPhone, $message);
                                        if ($sms['sent'] == true) {
                                            insert('tbl_transactions')->values($values);
                                            $lastInsertID = last_id();
                                            if ($lastInsertID > 0) {
                                                $result['success'] = true;
                                            } else {
                                                $result['success'] = false;
                                                $result['msg'] = 'Sorry! Couldn\'t complete your transaction. Please contact Surraf customer service';
                                            }
                                        } else {
                                            $result['success'] = false;
                                            $result['msg'] = $sms['msg'];
                                        }
                                    } else {
                                        $result['success'] = false;
                                        $result['msg'] = 'Oops!! Couldn\'t get Forex Exchange rate! Please contact Surraf customer support';
                                    }
                                }
                            } //Surraf Wallet to Bank
                            else if ($sendingAccType == 3 AND $receivingAccType == 1) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Surraf Wallet to Telco
                            else if ($sendingAccType == 3 AND $receivingAccType == 2) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Surraf Wallet to Card
                            else if ($sendingAccType == 3 AND $receivingAccType == 4) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Surraf Wallet to Paypal
                            else if ($sendingAccType == 3 AND $receivingAccType == 5) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Bank to Surraf Wallet
                            else if ($sendingAccType == 1 AND $receivingAccType == 3) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Telco to Surraf Wallet
                            else if ($sendingAccType == 2 AND $receivingAccType == 3) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Card to Surraf Wallet
                            else if ($sendingAccType == 4 AND $receivingAccType == 3) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Paypal to Surraf Wallet
                            else if ($sendingAccType == 5 AND $receivingAccType == 3) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Bank to Bank
                            else if ($sendingAccType == 1 AND $receivingAccType == 1) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Bank to Telco
                            else if ($sendingAccType == 1 AND $receivingAccType == 2) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Bank to Card
                            else if ($sendingAccType == 1 AND $receivingAccType == 4) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Bank to Paypal
                            else if ($sendingAccType == 1 AND $receivingAccType == 5) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Telco to Telco
                            else if ($sendingAccType == 2 AND $receivingAccType == 2) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Telco to Bank
                            else if ($sendingAccType == 2 AND $receivingAccType == 1) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Telco to Card
                            else if ($sendingAccType == 2 AND $receivingAccType == 4) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Telco to Paypal
                            else if ($sendingAccType == 2 AND $receivingAccType == 5) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Card to Card
                            else if ($sendingAccType == 4 AND $receivingAccType == 4) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Card to Bank
                            else if ($sendingAccType == 4 AND $receivingAccType == 1) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Card to Telco
                            else if ($sendingAccType == 4 AND $receivingAccType == 2) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Card to Paypal
                            else if ($sendingAccType == 4 AND $receivingAccType == 5) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Paypal to Paypal
                            else if ($sendingAccType == 5 AND $receivingAccType == 5) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Paypal to Bank
                            else if ($sendingAccType == 5 AND $receivingAccType == 1) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Paypal to Telco
                            else if ($sendingAccType == 5 AND $receivingAccType == 2) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } //Paypal to Card
                            else if ($sendingAccType == 5 AND $receivingAccType == 4) {
                                $result['success'] = false;
                                $result['msg'] = $InActiveMsg;
                            } else {
                                $result['success'] = false;
                                $result['msg'] = 'Sorry! Transaction not allowed.';
                            }
                        }
                    } else {
                        $result['success'] = false;
                        $result['msg'] = 'Sorry! You do not have enough balance to perform this transaction. Please top up your account and try again.';
                    }
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Sorry! There\'s an error with one of your accounts. Please contact Surraf customer service.';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});

//Send money confirmation
$app->post('/v1/complete_transfer', function (Request $request, Response $response) {
    $jwt = $request->getHeaders();
    $now = time();
    $key = "my_secret_key";
    $data = $request->getParsedBody();
    $sendingAccPIN = md5($data['accPIN']);
    $sendingAccID = intval($data['accID']);
    $transactionPIN = md5($data['txnPIN']);
    $username = "Acculynk";
    $apikey = "2961d831076d8a4abcb0218075dfaaaba267d7d7ebfb839889cfdc754594f8e7";
    $gateway = new AfricasTalkingGateway($username, $apikey);

    try {
        $decoded = JWT::decode($jwt['HTTP_AUTHORIZATION'][0], $key, array('HS256'));
        if (isset($decoded)) {
            $UID = select('tbl_access_tokens')->which("user_id")->where("user_id = " . $decoded->context->user->user_id . " AND expiration_date >= $now")->result('user_id');
            $SendingAccID = select('tbl_user_money_accounts')->which('id')->where("id= $sendingAccID AND userID = $UID AND accountPIN = '" . $sendingAccPIN . "'")->result('id');
            if ($UID > 0) {
                if ($SendingAccID > 0) {
                    //Transaction Data
                    $txnData = select('tbl_transactions')->which('id, trxFromCurrency, trxAmount, toAccount, trxToCurrency, trxActualAmount, txnReference')->where("fromAccount = $SendingAccID  AND transactionOTP = '" . $transactionPIN . "' AND trxStatus = 0")->result();

                    //If transaction exists
                    if (isset($txnData) AND !empty($txnData) AND count($txnData) > 0) {
                        $txnID = $txnData['id'];
                        $txnRef = $txnData['txnReference'];
                        $txnSendingCurrency = $txnData['trxFromCurrency'];
                        $txnSentAmount = $txnData['trxAmount'];
                        $txnRecipientAcc = $txnData['toAccount'];
                        $txnRecipientCurrency = $txnData['trxToCurrency'];
                        $txnReceivedAmount = $txnData['trxActualAmount'];

                        $SendingUser = select('tbl_system_users')->which('phoneNumber, firstName, middleName, lastName')->where("id = $UID AND isActive = 1")->result();
                        $ReceivingUID = select('tbl_user_money_accounts')->which('userID')->where("id = " . $txnRecipientAcc . " ")->result('userID');
                        $ReceivingUser = select('tbl_system_users')->which('phoneNumber, firstName, middleName, lastName, pushID')->where("id = " . $ReceivingUID . " AND isActive = 1")->result();

                        update('tbl_user_money_accounts')->decrease('accountActualBalance', $txnSentAmount)->where("id = $SendingAccID");
                        update('tbl_user_money_accounts')->increase('accountActualBalance', $txnReceivedAmount)->where("id = $txnRecipientAcc");
                        update('tbl_transactions')->values(array('trxStatus' => 1))->where("id = $txnID");
                        $GetTxnSeries = select('tbl_code_serialization')->which('codeSeries')->order('id DESC')->limit(1)->result('codeSeries');
                        insert('tbl_code_serialization')->values(array('codeSeries' => $GetTxnSeries + 1));
                        $NewRecipientAmount = select('tbl_user_money_accounts')->which('accountActualBalance')->where("id = $txnRecipientAcc")->result('accountActualBalance');

                        $SenderSMS = "Surraf TXN " . $txnRef . ". You have successfully transferred " . $txnSendingCurrency . " " . number_format($txnSentAmount, 2) . " to " . $ReceivingUser['firstName'] . " " . $ReceivingUser['middleName'] . " " . $ReceivingUser['lastName'] . ". Received amount will be " . $txnRecipientCurrency . " " . number_format($txnReceivedAmount, 2) . ".";
                        $RecipientSMS = "Surraf TXN " . $txnRef . ". You have received " . $txnRecipientCurrency . " " . number_format($txnReceivedAmount, 2) . " from " . $SendingUser['firstName'] . " " . $SendingUser['middleName'] . " " . $SendingUser['lastName'] . ". Your account balance is " . $txnRecipientCurrency . " " . number_format($NewRecipientAmount, 2) . ".";

                        sendConfirmationSMS($gateway, $SendingUser['phoneNumber'], $SenderSMS);
                        sendConfirmationSMS($gateway, $ReceivingUser['phoneNumber'], $RecipientSMS);
                        $pushResponse = sendPushNotification($RecipientSMS, $txnRecipientAcc, round($NewRecipientAmount,2), $ReceivingUser['pushID']);

                        $result['success'] = true;
                        $result['data'] = select('tbl_user_money_accounts')->which('ROUND(accountActualBalance, 2) AS AmountAvailable')->where("id =  $sendingAccID")->result();
                        $result['msg'] = 'Your Transaction  has been Successfully Completed. Thank you for using Surraf.';
                        $result['push'] = $pushResponse;
                    } else {
                        $result['success'] = false;
                        $result['msg'] = 'Wrong Transaction PIN!';
                    }
                } else {
                    $result['success'] = false;
                    $result['msg'] = 'Wrong Account PIN!';
                }
            } else {
                $result['success'] = false;
                $result['msg'] = 'User not authorized. Corrupt or expired authentication token! Please log in for a new one';
            }
        }
    } catch (UnexpectedValueException $e) {
        $result['success'] = false;
        $result['msg'] = 'Invalid authentication token!!.';
    }
    echo json_encode($result);
});
